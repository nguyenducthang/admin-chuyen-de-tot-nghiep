import React, { Component } from 'react'
import * as firebase from "firebase";
import { DropdownToggle,DropdownMenu, DropdownItem, ButtonDropdown, Table} from 'reactstrap';
import { Badge, FormGroup, Input, FormFeedback, Label, Modal, ModalBody, ModalFooter, ModalHeader, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row } from 'reactstrap';
import { Button} from 'reactstrap';
import { Alert } from 'rsuite';
import CartProduct from './CartProduct';
import { auth } from "./firebaseConnect";

export default class Order extends Component {
    constructor(props) {
        super(props); 
        this.state = {
          table:[],
          order:[],  
          large: false,
          dropdownOpen: new Array(19).fill(false),
        };
        this.toggle = this.toggle.bind(this);
        this.toggleLarge = this.toggleLarge.bind(this);
      }
    toggleLarge1=(data, tong)=> {
          this.setState({
            order: Object.values(data),
            tong: tong,
          });
          this.toggleLarge();
      }
    toggleLarge() {
        this.setState({
          large: !this.state.large,
        });
    }
    toggle(i) {
        const newArray = this.state.dropdownOpen.map((element, index) => { return (index === i ? !element : false); });
        this.setState({
          dropdownOpen: newArray,
        });
      }
    componentWillMount(){
        auth.onAuthStateChanged(userAuth => {
        this.setState({ user: userAuth});
        const {uid} = (userAuth);
        var arrayData=[];
        var dataOrder = firebase.database().ref("/Order/"+uid);
        dataOrder.once("value").then((snapshot)=>{
            if(snapshot.val() != null){
              snapshot.forEach(element =>{
                const keyOrder = element.key
                const Address = element.val().Address;
                const Name = element.val().Name;
                const TelephoneNumber = element.val().TelephoneNumber;
                const Note = element.val().Note;
                const Tong = element.val().Tong;
                const Time = element.val().Time;
                const Order = element.val().order;
                arrayData.push({
                  keyOrder: keyOrder,
                  Address: Address,
                  Name: Name,
                  TelephoneNumber: TelephoneNumber,
                  Note: Note,
                  Tong: Tong,
                  Time: Time,
                  Order: Order,
                })
                })
            this.setState({
              table: arrayData,
            });
          }
        });
      })
      }
    deleteData =(keyOrder)=>{
        const {uid} = (this.state.user);
        var dataCart = firebase.database().ref("/Order/"+uid);
        dataCart.child(keyOrder).remove().then(function(result) {
        Alert.success('Xóa đơn hàng thành công');
        })
        var arrayData=[];
        var dataOrder = firebase.database().ref("/Order/"+uid);
        dataOrder.once("value").then((snapshot)=>{
            if(snapshot.val() != null){
              snapshot.forEach(element =>{
                const keyOrder = element.key
                const Address = element.val().Address;
                const Name = element.val().Name;
                const TelephoneNumber = element.val().TelephoneNumber;
                const Note = element.val().Note;
                const Tong = element.val().Tong;
                const Time = element.val().Time;
                const Order = element.val().order;
                arrayData.push({
                  keyOrder: keyOrder,
                  Address: Address,
                  Name: Name,
                  TelephoneNumber: TelephoneNumber,
                  Note: Note,
                  Tong: Tong,
                  Time: Time,
                  Order: Order,
                })               
              })
            this.setState({
              table: arrayData,
            });
          }
        });
    }
    sortByPriceAsc=()=>{

        let sortedProductsAsc;
        sortedProductsAsc= this.state.table.sort((a,b)=>{
           return parseInt(Date.parse(a.Time).valueOf())  - parseInt(Date.parse(b.Time).valueOf());
        })
    
        this.setState({
            table:sortedProductsAsc
        }) 
    }
    
    
    sortByPriceDsc=()=>{
    
        let sortedProductsDsc;
        sortedProductsDsc= this.state.table.sort((a,b)=>{
           return parseInt(Date.parse(b.Time).valueOf())  - parseInt(Date.parse(a.Time).valueOf());
        })
    
        this.setState({
            table:sortedProductsDsc
        })
    }
    render() {
        return (
            <div>

            
                <Table responsive striped hover>
                  
                  <thead>
                   
                  <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Telephone number</th>
                    <th><ButtonDropdown className="mr-1" isOpen={this.state.dropdownOpen[1]} toggle={() => { this.toggle(1); }}>
                          <DropdownToggle caret color="info">
                          Time Order
                          </DropdownToggle>
                          <DropdownMenu>
                            <DropdownItem onClick={this.sortByPriceAsc}><i className="fas fa-sort-up fa-2x"></i>tăng dần</DropdownItem>
                            <DropdownItem onClick={this.sortByPriceDsc}><i className="fas fa-sort-down fa-2x"></i>giảm dần</DropdownItem>
                          </DropdownMenu>
                  </ButtonDropdown></th>
                    <th>Tong</th>
                    <th>Note</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>

        
                  {(this.state.table).map((value,key)=>{
                              return (
                                
                                <tr>
                                <td onClick={()=>this.toggleLarge1(value.Order, value.Tong)}>{value.Name}</td>
                                <td onClick={()=>this.toggleLarge1(value.Order, value.Tong)}>{value.Address}</td>
                                <td onClick={()=>this.toggleLarge1(value.Order, value.Tong)}>{value.TelephoneNumber}</td>
                                <td onClick={()=>this.toggleLarge1(value.Order, value.Tong)}>{value.Time}</td>   
                                <td onClick={()=>this.toggleLarge1(value.Order, value.Tong)}>{value.Tong.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>     
                                <td onClick={()=>this.toggleLarge1(value.Order, value.Tong)}>{value.Note}</td> 
                                <td>
                                <Button color="warning" className="btn-pill" onClick={()=>this.deleteData(value.keyOrder)}>
                                <i className ="fas fa-trash-alt"></i>&nbsp;Delete
                                </Button>
                                </td>
                                </tr>
                                  
                                  )
                          })}

                  
                  </tbody>
                </Table>
                <Modal isOpen={this.state.large} toggle={this.toggleLarge}
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader toggle={this.toggleLarge}><div className="title-box">
                    <h4>Order</h4>
                  </div></ModalHeader>
                  <ModalBody>
                  
                  <div className="container">
                  
                  <div className="container">
                  <div className="row">
                  
                  {(this.state.order).map((value,key)=>{
                              
                            return (
                              <div className="col-md-3 col-6">
                                <div className="product-top text-center">
                                <img src={value.anh} alt="anh" /></div>
                                  <div className="text-center" >
                                  <label>Số lượng: {value.quantity}</label><br/></div>
                                  <div className="product-bottom text-center">
                                  <h5>{value.ten}</h5>
                                  <h5> {[value.gia].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}đ</h5>
                                </div>
                              </div>

                                
                                )
                        })}
                        </div></div>
                </div>
                 
                 
                  </ModalBody>
                  <ModalFooter>
                    <h4>Tổng tiền đơn hàng: {[this.state.tong].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}đ</h4>
                    <Button color="primary" onClick={this.toggleLarge}>Ok</Button>
                    <Button color="secondary" onClick={this.toggleLarge}>Cancel</Button>
                  </ModalFooter>
                </Modal>
            </div>
        )
    }
}
