import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import React, { Component } from 'react';
export class MapContainer extends Component {
    state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
    };
   
    onMarkerClick = (props, marker, e) =>
      this.setState({
        selectedPlace: props,
        activeMarker: marker,
        showingInfoWindow: true
      });
   
    onMapClicked = (props) => {
      if (this.state.showingInfoWindow) {
        this.setState({
          showingInfoWindow: false,
          activeMarker: null
        })
      }
    };
   
    render() {
        const style = {
            width: '60vw',
            height: '60vh'
          }
      return (
          <div style={style}>
        <Map google={this.props.google} zoom={15}
            onClick={this.onMapClicked}
            initialCenter={{
                lat: 21.0004058,
                lng: 105.8422492
              }}>
          <Marker onClick={this.onMarkerClick}
                  name={'Current location'} />
   
          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}>
              <div>
                <h1>{this.state.selectedPlace.name}</h1>
              </div>
          </InfoWindow>
        </Map>
        </div>
      )
    }
  }
 
export default GoogleApiWrapper({
  apiKey: "AIzaSyBJhwx3T8ZVRIe7leVtvFVrSaKXXivPol4"
})(MapContainer)