import React, { useContext } from "react";
import { UserContext } from "./UserProvider";
import {auth} from "./firebaseConnect";
import { Alert } from 'rsuite';
import {
  BrowserRouter as Router,
  useHistory,
  } from "react-router-dom";
  
const ProfilePage = () => {
  let history = useHistory();
  const user = useContext(UserContext);
  const {photoURL, displayName, email} = user;
  return (
    <div className = "mx-auto w-12/12 md:w-6/6 py-8 px-4 md:px-8">
      <div className="flex border flex-col items-center md:flex-row md:items-start border-blue-400 px-3 py-4">
        <div
          style={{
            background: `url(${photoURL || 'https://res.cloudinary.com/dqcsk8rsc/image/upload/v1577268053/avatar-1-bitmoji_upgwhc.png'})  no-repeat center center`,
            backgroundSize: "cover",
            height: "150px",
            width: "150px"
          }}
          className="border border-blue-300"
        ></div>
        <div className = "md:pl-4">
        <h2 className = "text-2xl font-semibold">{displayName}</h2>
        <h6 className = "italic">{email}</h6>
        </div>
      </div>
      <button className = "w-full py-3 bg-red-600 mt-4 text-white" onClick = {() => {auth.signOut().then(function() {
  Alert.success('Đăng xuất thành công');
  setTimeout(() => history.push("/"), 100);
})}}>Sign out</button>
    </div>
  ) 
};
export default ProfilePage;