import React, { Component } from 'react';
import {
  Link
} from "react-router-dom";
import * as firebase from "firebase";
import { Alert } from 'rsuite';
import { auth } from "./firebaseConnect";
import { Button, FormGroup, Col} from 'reactstrap';

class CartProduct extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      user: null,
    };
  }
  
  componentWillMount(){
    auth.onAuthStateChanged(userAuth => {
    this.setState({ user: userAuth});
  //   if(this.state.user != null){
  //   const {uid} = (userAuth);
  //   var dataCart = firebase.database().ref("/Cart/"+uid+"/"+this.props.keyid);
  //   dataCart.once("value").then((snapshot)=>{
  //     if(snapshot.val() != null){console.log(snapshot.val())
  //     this.setState({
  //       cart: snapshot.val(),
  //       value:(this.props.soluong)
  //     });   }
  // });}
  })
  }
  isChange = (event) =>{
    const val = event.target.value;
    this.props.addProduct(val,this.props.keyid)
  }

  chuyenURL=(str)=>{
     // Chuyển hết sang chữ thường
     str = str.toLowerCase();     
   
     // xóa dấu
     str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
     str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
     str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
     str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
     str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
     str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
     str = str.replace(/(đ)/g, 'd');
  
     // Xóa ký tự đặc biệt
     str = str.replace(/([^0-9a-z-\s])/g, '');
  
     // Xóa khoảng trắng thay bằng ký tự -
     str = str.replace(/(\s+)/g, '-');
  
     // xóa phần dự - ở đầu
     str = str.replace(/^-+/g, '');
  
     // xóa phần dư - ở cuối
     str = str.replace(/-+$/g, '');
  
     // return
     return str;
  }
    render() {
        return (
            
                <div className="col-md-3 col-6">
          <div className="product-top text-center">
          <Link to={"/product/"+this.chuyenURL(this.props.tensp)+"."+(this.props.tinid)}><img src={this.props.anhsp} alt="anh" /></Link>
            <div className="overlay-right">
            <Link to={"/product/"+this.chuyenURL(this.props.tensp)+"."+(this.props.tinid)}><button type="button" className="btn btn-secondary" title="Quick Shop">
                <i className="fa fa-eye" />
              </button></Link>
              <button onClick={()=>this.props.deleteData(this.props.keyid)} type="button" className="btn btn-secondary" title="Delete">
                <i className="fas fa-trash-alt" />
              </button>
              <button type="button" className="btn btn-secondary" title="Add to cart">
                <i className="fa fa-shopping-cart" />
              </button>
            </div></div>
            <div className="text-center" >
            <label>Số lượng: </label><br/>
            <FormGroup row>                
        <Col xs="4"><Button color="info" size="sm" className="btn-pill" onClick={() => this.props.addProduct(this.props.soluong-1,this.props.keyid)}>-</Button></Col>
        <Col xs="4"><input onChange={(event)=>this.isChange(event)} id="soluong" type="text" value={this.props.soluong} /></Col>
        <Col xs="3"><Button color="info" size="sm" className="btn-pill" onClick={() => this.props.addProduct(this.props.soluong+1,this.props.keyid)}>+</Button></Col>
        </FormGroup></div>
             <div className="product-bottom text-center">
            <i className="fa fa-star" />
            <i className="fa fa-star" />
            <i className="fa fa-star" />
            <i className="fa fa-star" />
            <i className="fa fa-star-half-o" />
            <h5>{this.props.tensp}</h5>
            <h5> {(this.props.giasp).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}đ</h5>
          </div>
        </div>
            
        );
    }
}

export default CartProduct;