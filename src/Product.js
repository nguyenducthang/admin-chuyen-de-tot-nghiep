import React, { Component } from 'react';
import * as firebase from "firebase";
import {
  Link,
  BrowserRouter as Router,
  useParams,
  Switch,
  Route,
} from "react-router-dom";
import { auth } from "./firebaseConnect";
import { Alert } from 'rsuite';

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      table:[],
      value: 1,
      user: null,
      cart:[],
    };
  }
  componentWillMount(){
    var dataSanPham = firebase.database().ref("/NewProduct");
    dataSanPham.once("value").then((snapshot)=>{
      this.setState({
        table: snapshot.val()
      });
  });
  
    auth.onAuthStateChanged(userAuth => {
    this.setState({ user: userAuth});
    if(userAuth != null){
    const {uid} = (userAuth);
    var dataCart = firebase.database().ref("/Cart/"+uid);
    dataCart.once("value").then((snapshot)=>{
      if(snapshot.val() != null){
      this.setState({
        cart: snapshot.val()
      });  }    
  });}
  });
  }
  addProduct=(anh, ten, gia)=>{
    if(this.state.user == null){Alert.warning('Bạn chưa đăng nhập');}
    else{
    var paramProduct =  window.location.pathname;
    const {uid} = (this.state.user);
    var dataCart = firebase.database().ref("/Cart/"+uid);
      dataCart.push({
      idProduct:(paramProduct.slice(paramProduct.indexOf(".")+1)),
      quantity:(this.state.value),
      anh:anh,
      ten:ten,
      gia:gia,
    }); 
    this.componentWillMount();
    Alert.success('Thêm vào giỏ hàng thành công');
  }}
  isChange = (event) =>{
    const val = event.target.value;
    this.setState({
      value: val,
    });
  }

    render() {
      var paramProduct =  window.location.pathname;
      
        return (
            <div>

{(this.state.table).map((value,key)=>{
        if(key == paramProduct.slice(paramProduct.indexOf(".")+1)){
          return(
          <div>
          {/*------------------product---------------------*/}
  <section className="single-product">
  <div className="container">
    <div className="row">
      <div className="col-md-5">
        <div id="product-slider" className="carousel slide carousel-fade" data-ride="carousel">
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img src={value.anh.photo1} className="d-block w-100" alt="anh" />
            </div>
            <div className="carousel-item">
              <img src={value.anh.photo2} className="d-block w-100" alt="anh" />
            </div>
            <div className="carousel-item ">
              <img src={value.anh.photo3} className="d-block w-100" alt="anh" />
            </div>
          </div>
          <a className="carousel-control-prev" href="#product-slider" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true" />
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href="#product-slider" role="button" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true" />
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div>

      <div className="col-md-7">
        <p className="new-arrival text-center">NEW</p>
          <h2>{value.ten}</h2>
        <i className="fa fa-star" />
        <i className="fa fa-star" />
        <i className="fa fa-star" />
        <i className="fa fa-star" />
        <i className="fa fa-star-o" />
        <p className="price"> {(value.gia).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}đ</p>{()=>this.ParamsExample()}
        <label>Size áo: </label>
        <div className="custom-control custom-radio custom-control-inline form-check">
          <input type="radio" id="customRadioInline1" name="customRadioInline1" className="custom-control-input"/>
          <label className="custom-control-label" htmlFor="customRadioInline1" active>S</label>
        </div>
        <div className="custom-control custom-radio custom-control-inline form-check">
          <input type="radio" id="customRadioInline2" name="customRadioInline1" className="custom-control-input "/>
          <label className="custom-control-label" htmlFor="customRadioInline2">M</label>
        </div>
        <div className="custom-control custom-radio custom-control-inline form-check">
          <input type="radio" id="customRadioInline3" name="customRadioInline1" className="custom-control-input"/>
          <label className="custom-control-label" htmlFor="customRadioInline3">L</label>
        </div>
        <div className="custom-control custom-radio custom-control-inline form-check">
          <input type="radio" id="customRadioInline4" name="customRadioInline1" className="custom-control-input" />
          <label className="custom-control-label" htmlFor="customRadioInline4">XL</label>
        </div>
        <br /><label>Số lượng: </label>
        <button id="btnCut" className="btn btn-info" onClick={() =>{ if((this.state.value)>1){ this.setState((state)=>({value:state.value - 1}))}}}>-</button>
        <input onChange={(event)=>this.isChange(event)} id="soluong" type="text" value={this.state.value} />
        <button id="btnAdd" className="btn btn-info" onClick={() => this.setState((state)=>({value:state.value -1 +2}))}>+</button>
        <br /><br /><button id="btn1" className="btn btn-primary" onClick={()=>this.addProduct(value.anh.photo1,value.ten,value.gia)} >Add to cart</button>
      </div>
    </div>
  </div>
</section>
{/*----------------product description-----------------------*/}

<section className= "product-description" >
  <div className="container">
    <h6>Mô tả sản phẩm </h6>
    <p>
      {value.content}
    </p>
  </div> </section></div>)
        }
      })}

  
</div>

        );
    }
}

export default Product;

