import React, { Component } from 'react';
import { Button, Card, CardBody, CardHeader, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import {
  Link,
} from "react-router-dom";
import GoogleApi from './GoogleApi';
class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      large: false,
    };
    this.toggleLarge = this.toggleLarge.bind(this);
  }
  toggleLarge() {
    this.setState({
      large: !this.state.large,
    });
  }
    render() {
        return (
            <div>
               {/*----------footer--------------------*/}
<section className="footer">
  <div className="container text-center">
    <div className="row">
      <div className="col-md-3 col-6 ">
        <h1>Công ty </h1>
        <p> Về chúng tôi</p>
        <p> Tuyển dụng</p>
      </div>
      <div className="col-md-3 col-6">
        <h1>Theo dõi chúng tôi </h1>
        <a href="https://www.facebook.com/profile.php?id=100006604869746"><p><i className="fa fa-facebook-official" /> Facebook</p></a>
        <p><i className="fa fa-youtube-play" /> YouTube</p>
        <p><i className="fa fa-twitter" /> Twitter</p>
      </div>
      <div className="col-md-3 col-6" onClick={this.toggleLarge}>
      <h1 > Hệ thống cửa hàng</h1>
      <Link><i class="fas fa-map-marked-alt fa-3x"></i></Link>
      </div>
      <div className="col-md-3 col-6 footer-image ">
        <h1>Tải xuống </h1>
        <a href="https://play.google.com/store?hl=vi"><img src="https://media04.lokalkompass.de/article/2015/12/16/6/7752516_XXL.jpg" alt="anh" /></a>
      </div>
    </div>
  </div>
</section>


<Modal isOpen={this.state.large} toggle={this.toggleLarge}
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader toggle={this.toggleLarge}>
                  </ModalHeader>

                  
                  

                    <GoogleApi></GoogleApi>
 
                 
   
                </Modal>
 
            </div>
        );
    }
}

export default Footer;