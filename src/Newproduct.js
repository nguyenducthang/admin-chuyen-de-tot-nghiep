import React, { Component } from 'react';
import {
  Link,
} from "react-router-dom";
import * as firebase from "firebase";
import { Alert } from 'rsuite';
import { auth } from "./firebaseConnect";


class Newproduct extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      cart:[],
      user: null
    };
  }
  
  componentWillMount(){
    auth.onAuthStateChanged(userAuth => {
    this.setState({ user: userAuth});
    if(this.state.user != null){
    const {uid} = (userAuth);
    var dataCart = firebase.database().ref("/Cart/"+uid);
    dataCart.once("value").then((snapshot)=>{
      if(snapshot.val() != null){
      this.setState({
        cart: snapshot.val()
      });   }
  });}
  })
  }
  
  addProduct=()=>{
    if(this.state.user == null){
      Alert.warning('Bạn chưa đăng nhập');
    }else{
    const {uid} = (this.state.user);
    var dataCart = firebase.database().ref("/Cart/"+uid);
    
      dataCart.push({
      idProduct:(this.props.tinid),
      quantity:1,
      anh:(this.props.anhsp),
      ten:(this.props.tensp),
      gia:(this.props.giasp),
    });
    Alert.success('Thêm vào giỏ hàng thành công')
  }}
  
  chuyenURL=(str)=>{
     // Chuyển hết sang chữ thường
     str = str.toLowerCase();     
   
     // xóa dấu
     str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
     str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
     str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
     str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
     str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
     str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
     str = str.replace(/(đ)/g, 'd');
  
     // Xóa ký tự đặc biệt
     str = str.replace(/([^0-9a-z-\s])/g, '');
  
     // Xóa khoảng trắng thay bằng ký tự -
     str = str.replace(/(\s+)/g, '-');
  
     // xóa phần dự - ở đầu
     str = str.replace(/^-+/g, '');
  
     // xóa phần dư - ở cuối
     str = str.replace(/-+$/g, '');
  
     // return
     return str;
  }
    render() {
        return (
            
                <div className="col-md-3 col-6">
          <div className="product-top">
          <Link to={"/product/"+this.chuyenURL(this.props.tensp)+"."+(this.props.tinid)}><img src={this.props.anhsp} alt="anh" /></Link>
            <div className="overlay-right">
              <Link to={"/product/"+this.chuyenURL(this.props.tensp)+"."+(this.props.tinid)}>
              <button type="button" className="btn btn-secondary" title="Quick Shop">
                <i className="fa fa-eye" />
              </button></Link>
              <button type="button" className="btn btn-secondary" title="Add to wishlist">
                <i className="fa fa-heart-o" />
              </button>
              <button onClick={()=>this.addProduct()} type="button" className="btn btn-secondary" title="Add to cart">
                <i className="fa fa-shopping-cart" />
              </button>
            </div></div>
          <div className="product-bottom text-center">
            <i className="fa fa-star" />
            <i className="fa fa-star" />
            <i className="fa fa-star" />
            <i className="fa fa-star" />
            <i className="fa fa-star-half-o" />
            <h3>{this.props.tensp}</h3>
            <h5> {(this.props.giasp).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}đ</h5>
          </div>
        </div>
            
        );
    }
}

export default Newproduct;