import React, { Component } from 'react';
import * as firebase from "firebase";

class Headermenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      table:[],

    };
  }
  componentWillMount(){
    var dataSanPham = firebase.database().ref("/CarouselPhoto");
    dataSanPham.once("value").then((snapshot)=>{
      this.setState({
        table: snapshot.val()
      });
  });
  }
    render() {
        return (
            <div>
  <section className="header">
    <div className="side-menu" id="side-menu">
      <ul>
        <li>On sale<i className="fa fa-cloud" />
          <ul>
            <li>Sub menu1</li>
            <li>Sub menu1</li>
            <li>Sub menu1</li>
            <li>Sub menu1</li>
          </ul>
        </li>
        <li>Mobiles<i className="fa fa-cloud" />
          <ul>
            <li>Sub menu2</li>
            <li>Sub menu2</li>
            <li>Sub menu2</li>
            <li>Sub menu2</li>
          </ul></li>
        <li>Computer<i className="fa fa-cloud" />
          <ul>
            <li>Sub menu3</li>
            <li>Sub menu3</li>
            <li>Sub menu3</li>
            <li>Sub menu3</li>
          </ul></li>
      </ul>
    </div>
    <div className="slider">
      <div id="slider" className="carousel slide carousel-fade" data-ride="carousel">
        <div className="carousel-inner">
          <div className="carousel-item ">
            <img src="https://images.pexels.com/photos/291762/pexels-photo-291762.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260" className="d-block w-100" alt="anh"/>
          </div>
          <div className="carousel-item ">
            <img src="https://images.pexels.com/photos/996329/pexels-photo-996329.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260" className="d-block w-100" alt="anh" />  
            
          </div>
          <div className="carousel-item active">
            <img src="https://images.pexels.com/photos/1488463/pexels-photo-1488463.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260" className="d-block w-100" alt="anh" />
          </div>
        </div>
        <a className="carousel-control-prev" href="#slider" role="button" data-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true" />
          <span className="sr-only">Previous</span>
        </a>
        <a className="carousel-control-next" href="#slider" role="button" data-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true" />
        <span className="sr-only">Next</span>
        </a>
        <ol className="carousel-indicators">
          <li data-target="#slider" data-slide-to={0} className="active" />
          <li data-target="#slider" data-slide-to={1} />
          <li data-target="#slider" data-slide-to={2} />
        </ol>
      </div>
    </div>
  </section>
</div>

        );
    }
}

export default Headermenu;