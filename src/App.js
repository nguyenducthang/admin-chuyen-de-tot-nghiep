import React, {Component} from 'react';
import './App.css';
import {firebaseData} from "./firebaseConnect";
import RouterURL from './RouterURL';
import {
  BrowserRouter as Router
} from "react-router-dom";
import UserProvider from './UserProvider';
import 'rsuite/dist/styles/rsuite-default.css'; // or 'rsuite/dist/styles/rsuite-default.css'
class App extends Component {
  render(){
 
  return (
    <UserProvider>
    <Router>
    <div className="App">
      
      <RouterURL></RouterURL>
      {/*<Headermenu></Headermenu>
      <Categories></Categories>
      <Sale></Sale>
      <New></New>
      <Product></Product>*/}
      
    </div>
    </Router>
    </UserProvider>
  );
}}

export default App;
