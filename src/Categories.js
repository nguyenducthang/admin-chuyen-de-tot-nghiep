import React, { Component } from 'react';
import { Alert } from 'rsuite';
import * as firebase from "firebase";
class Categories extends Component {
    constructor(props) {
        super(props); 
        this.state = {
          table:[],
      
        };
      }
    componentWillMount(){
        var dataSanPham = firebase.database().ref("/CategoriesPhoto");
        dataSanPham.once("value").then((snapshot)=>{
          this.setState({
            table: snapshot.val()
          });
      });
      }
    render() {
        return (
            <div>
                {/*-----------------categories---------------*/}
                <section className="featured-categories">
                <div className="container">
                    <div className="row">
                    {(this.state.table).map((value,key)=>{
                return (
                    <div className="col" title="Hàng sắp về">
                        <img src={value.photo} alt="anh"  /></div>
                    )
                     })}
                    
                    </div>
                </div>
                </section>

            </div>
        );
    }
}

export default Categories;