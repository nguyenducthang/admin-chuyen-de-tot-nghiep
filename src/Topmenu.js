import React, { Component, useContext } from 'react';
import { Button, Card, CardBody, CardHeader, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Badge,Input,  InputGroup,
  InputGroupAddon,  FormGroup, Form,
  InputGroupText, Table } from 'reactstrap';
import axios from 'axios';
import ReactHtmlParser from 'react-html-parser';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
import SignIn from './SignIn';
import ProfilePage from './ProfilePage';
import { UserContext } from "./UserProvider";
import CartProduct from './CartProduct';
import * as firebase from "firebase";
import { auth } from "./firebaseConnect";
import { Alert } from 'rsuite';
import { FormFeedback, Label, Col, Pagination, PaginationItem, PaginationLink, Row } from 'reactstrap';
import Order from './Order';

class Topmenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: "",
      large: false,
      LargeOrder: false,
      success: false,
      info: false,
      table:[],
      object:{},
      user: null,
      Tong:0,
      Dem:0,
    };
    this.toggleInfo = this.toggleInfo.bind(this);
    this.toggleLargeOrder = this.toggleLargeOrder.bind(this);
    this.toggleLarge = this.toggleLarge.bind(this);
    this.toggleSuccess = this.toggleSuccess.bind(this);
  }
  componentWillMount(){
    auth.onAuthStateChanged(userAuth => {
    this.setState({ user: userAuth});
    if(userAuth != null){
    const {uid} = (userAuth);
    var arrayData=[];
    var dataSanPham = firebase.database().ref("/Cart/"+uid);
    dataSanPham.once("value").then((snapshot)=>{
      if(snapshot.val() != null){
        this.setState({
          object: snapshot.val(),
        });
        snapshot.forEach(element =>{
          const key = element.key;
          const idProduct = element.val().idProduct;
          const anh = element.val().anh;
          const gia = element.val().gia;
          const quantity = element.val().quantity;
          const ten = element.val().ten;
          arrayData.push({
            key:key,
            idProduct: idProduct,
            anh:anh,
            gia:gia,
            quantity:quantity,
            ten:ten,
          })
        })
      this.setState({
        table: arrayData,
      });
    }
  });}
  });
    
  }
  toggleLarge() {
    if(this.state.user == null){
      Alert.warning('Bạn chưa đăng nhập');}
      else{
    this.setState({
      large: !this.state.large,
    });
    const {uid} = (this.state.user);
    var arrayData=[];
    var dataSanPham = firebase.database().ref("/Cart/"+uid);
    dataSanPham.once("value").then((snapshot)=>{
      if(snapshot.val() != null){
        this.setState({
          object: snapshot.val(),
        });
        snapshot.forEach(element =>{
          const key = element.key;
          const idProduct = element.val().idProduct;
          const anh = element.val().anh;
          const gia = element.val().gia;
          const quantity = element.val().quantity;
          const ten = element.val().ten;
          arrayData.push({
            key:key,
            idProduct: idProduct,
            anh:anh,
            gia:gia,
            quantity:quantity,
            ten:ten,
          })
        })
      this.setState({
        table: arrayData,
      });
    }
  });
  }}
  isChange = (event) =>{
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]:value
    });
  }
  addOrder=()=>{
    const {uid} = (this.state.user);
    var d = new Date();
    var dataOrder = firebase.database().ref("/Order/"+uid);
      dataOrder.push({
      Name:this.state.nameinput,
      TelephoneNumber:(this.state.telephoneNumber),
      Address:this.state.textareaAddress,
      Note:this.state.textareaNote,
      order:(this.state.object),
      Time:(d.toLocaleString()),
      Tong:this.state.Tong,
    }); 
    this.toggleInfo();
    Alert.success('Đặt hàng thành công');
    var dataCart = firebase.database().ref("/Cart");
    dataCart.child(uid).remove();
  }
  toggleInfo() {
    if(this.state.Tong == 0){Alert.warning('Bạn chưa có sản phẩm trong giỏ');}
    else{
      this.toggleLarge();
    this.setState({
      info: !this.state.info,
    });}
  }
  toggleLargeOrder() {
      this.toggleLarge();
    this.setState({
      LargeOrder: !this.state.LargeOrder,
    });
  }
  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }
  proFile=() =>{
    const user = useContext(UserContext);
    if(user==null){
      return(<div className="flex border flex-col items-center"><h2>Bạn chưa đăng nhập</h2><Link to="/signIn"><Button color="success" onClick={this.toggleSuccess}><i class="fa fa-sign-in-alt"></i>&nbsp;Log in</Button></Link></div>)
    }else{
      return(<div><ProfilePage/></div>)
    }
  }
  signIn=() =>{
    const user = useContext(UserContext);
    if(user!=null){
      return(null)
    }else{
      return(<div><Link to="/signIn"><li><a><i className="fa fa-sign-in-alt"></i>&nbsp;<strong id="text">Sign in</strong></a></li></Link></div>)
    }
  }
  addProduct=(number,key)=>{
    if(number <= 0){this.deleteData(key)}
    else{
    const {uid} = (this.state.user);
    var dataCart = firebase.database().ref("/Cart/"+uid+"/"+key);
      dataCart.child("quantity").set(number); 
      var arrayData=[];
    var dataSanPham = firebase.database().ref("/Cart/"+uid);
    dataSanPham.once("value").then((snapshot)=>{
      if(snapshot.val() != null){
        snapshot.forEach(element =>{
          const key = element.key;
          const idProduct = element.val().idProduct;
          const anh = element.val().anh;
          const gia = element.val().gia;
          const quantity = element.val().quantity;
          const ten = element.val().ten;
          arrayData.push({
            key:key,
            idProduct: idProduct,
            anh:anh,
            gia:gia,
            quantity:quantity,
            ten:ten,
          })
        })
      this.setState({
        table: arrayData,
      });
    }
  });
  }}
  deleteData =(id)=>{
    const {uid} = (this.state.user);
    var dataCart = firebase.database().ref("/Cart/"+uid);
    dataCart.child(id).remove().then(function(result) {
    Alert.success('Xóa sản phẩm thành công');
    })
    var arrayData=[];
    var dataSanPham = firebase.database().ref("/Cart/"+uid);
    dataSanPham.once("value").then((snapshot)=>{
      if(snapshot.val() != null){
        snapshot.forEach(element =>{
          const key = element.key;
          const idProduct = element.val().idProduct;
          const anh = element.val().anh;
          const gia = element.val().gia;
          const quantity = element.val().quantity;
          const ten = element.val().ten;
          arrayData.push({
            key:key,
            idProduct: idProduct,
            anh:anh,
            gia:gia,
            quantity:quantity,
            ten:ten,
          })
        })
      this.setState({
        table: arrayData,
      });
    }
  });
   }
  componentDidMount() {
    axios.get(`https://devayaloyalty-api.ayainnovation.com/api/user/create-captcha`)
      .then(res => {
        const persons = res.data;
        this.setState({ persons });
      })
  }
    render() {
      // const {uid} = (this.state.user);
      {this.state.Tong=0;
        (this.state.table).map((value,key)=>{this.state.Tong=this.state.Tong+value.gia*value.quantity})};
        {this.state.Dem=0;
          (this.state.table).map((value,key)=>{this.state.Dem=this.state.Dem+value.quantity})}
        return (
            <div >
                  <div className="top-nav-bar">
    <div className="search-box">
      <i className="fa fa-bars" id="menu-btn" onClick="openmenu()" />
      <i className="fa fa-times" id="close-btn" onClick="closemenu()" />
      <Link to="/"><img src="./../images/logo.jpg" className="logo" alt="anh" /></Link>
      <input type="text" className="form-control" />
      <span className="input-group-text"><i className="fa fa-search" /></span>
    </div>
    <div className="menu-bar float-sm-right ">
      <ul className="nowrap">
        <li className="cart" onClick={this.toggleLarge}><a><span className="badge badge-pill badge-light" id="text">{this.state.Dem}</span><i className="fa fa-shopping-basket" /><strong id="text">Cart</strong></a></li>
        <this.signIn/>
        <li className="login" onClick={this.toggleSuccess}><a><i className="fa fa-user"></i>&nbsp;<strong id="text">my Profile</strong></a></li>
      </ul>
    </div>
  </div>

                <Modal isOpen={this.state.large} toggle={this.toggleLarge}
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader toggle={this.toggleLarge}>
                  <div class="container">
                  <div class="row">
                    <div class="col-sm-8">
                  <div className="title-box">
                    <h4>Cart</h4>  
                  </div></div>
                  <div class="col-sm-4">
                  <Button color="primary" onClick={this.toggleLargeOrder}>Đơn hàng đã đặt</Button></div></div></div>
                  </ModalHeader>
                  <ModalBody>
                  
                  <div className="container">
                  
                  <div className="container">
                  <div className="row">
                  
                  {(this.state.table).map((value,key)=>{
                              
                            return (
                              
                                <CartProduct key={key}
                                    keyid={value.key}
                                    tinid={value.idProduct}
                                    anhsp={value.anh}
                                    tensp={value.ten}
                                    giasp={value.gia}
                                    soluong={value.quantity}
                                    deleteData={(id=>this.deleteData(id))}
                                    addProduct={((number,key)=>this.addProduct(number,key))}
                                    >
                                </CartProduct>
                                
                                )
                        })}
                        </div></div>
                </div>
                 
                 
                  </ModalBody>
                  <ModalFooter>
                    <h4>Tổng tiền đơn hàng: {(this.state.Tong).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}đ </h4>
                    <Button color="primary" onClick={this.toggleInfo}>Order</Button>
                    <Button color="secondary" onClick={this.toggleLarge}>Cancel</Button>
                  </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                       className={'modal-success ' + this.props.className}>
             
              
              <this.proFile></this.proFile>
              
           
                  <ModalFooter>
                    <Button color="success" onClick={this.toggleSuccess}><i class="fa fa-backward"></i>&nbsp;Back</Button>
                    <Button color="secondary" onClick={this.toggleSuccess}><i class="fa fa-power-off"></i>&nbsp;Close</Button>
                  </ModalFooter>
                </Modal>
            
                <Modal isOpen={this.state.info} toggle={this.toggleInfo}
                       className={'modal-info ' + this.props.className}>
                  <ModalHeader toggle={this.toggleInfo}>Order</ModalHeader>
                  <ModalBody>
                  
                    <CardBody>
                      <Row className="was-validated">
                      </Row>

                      <FormGroup row>
                        <Col md="2">
                          <Label htmlFor="name-input">Name </Label>
                        </Col>
                        <Col xs="12" md="10">
                          <Input onChange={(event)=>this.isChange(event)} type="text" id="name" name="nameinput" placeholder="Your name" />
                        </Col>
                      </FormGroup>


                      <FormGroup row>
                        <Col md="2">
                          <Label htmlFor="price-input">Telephone number</Label>
                        </Col>
                        <Col xs="12" md="10">
                          <Input onChange={(event)=>this.isChange(event)} type="text" id="price" name="telephoneNumber" placeholder="Telephone number" />
                        </Col>
                      </FormGroup>

                      <FormGroup row>
                        <Col md="2">
                          <Label htmlFor="textarea-input">Address</Label>
                        </Col>
                        <Col xs="12" md="10">
                          <Input  onChange={(event)=>this.isChange(event)} type="textarea" name="textareaAddress" id="textarea-input" rows="1"
                                placeholder="Your address" />
                        </Col>
                      </FormGroup>

                      <FormGroup row>
                        <Col md="2">
                          <Label htmlFor="textarea-input">Note</Label>
                        </Col>
                        <Col xs="12" md="10">
                          <Input  onChange={(event)=>this.isChange(event)} type="textarea" name="textareaNote" id="textarea-input" rows="3"
                                placeholder="Note..." />
                        </Col>
                      </FormGroup>

                      


                    </CardBody>
                  
                  </ModalBody>
                  <ModalFooter>
                    <Button color="success" onClick={()=>this.addOrder()}><i className="fa fa-paper-plane"></i>&nbsp;Buy</Button>
                    <Button color="secondary" onClick={this.toggleInfo}>Cancel</Button>
                  </ModalFooter>
                </Modal>
            
                <Modal isOpen={this.state.LargeOrder} toggle={this.toggleLargeOrder}
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader toggle={this.toggleLargeOrder}>Đơn đã đặt</ModalHeader>
                  <ModalBody>
                  
                    <CardBody>
                        <Order/>
                    </CardBody>
                  
                  </ModalBody>
                  <ModalFooter>
                    <Button color="success" onClick={this.toggleLargeOrder}>Ok</Button>
                    <Button color="secondary" onClick={this.toggleLargeOrder}>Cancel</Button>
                  </ModalFooter>
                </Modal>
            
            
            </div>
        );
    }
}

export default Topmenu;