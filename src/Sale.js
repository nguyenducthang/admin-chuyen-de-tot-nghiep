import React, { Component } from 'react';
import * as firebase from "firebase";
import {
  Link
} from "react-router-dom";
import Newproduct from './Newproduct';
class Sale extends Component {
  constructor(props) {
    super(props);
    this.state = {
        table:[],
      };    
}
componentWillMount(){
  var dataSanPham = firebase.database().ref("/NewProduct");
  dataSanPham.once("value").then((snapshot)=>{
    this.setState({
      table: snapshot.val()
    });
});
}
    render() {
        return (
            <div>
              {/*----------------Sale product-------------*/}
<section className="on-sale">
  <div className="container">
    <div className="title-box">
      <h2>On sale</h2>
    </div>
    <div className="row">
    {(this.state.table).map((value,key)=>{
                return (
                    <Newproduct key={key}
                        tinid={key}
                        anhsp={value.anh.photo1}
                        tensp={value.ten}
                        giasp={value.gia}>
                    </Newproduct>
                    )
            })}
            
    </div>
  </div>
</section>
  
            </div>
        );
    }
}

export default Sale;