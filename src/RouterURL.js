import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";
import Headermenu from './Headermenu';
import Product from './Product';
import Categories from './Categories';
import Sale from './Sale';
import New from './New';
import Footer from './Footer';
import Topmenu from './Topmenu';
import SignIn from './SignIn';
import SignUp from './SignUp';
import ProfilePage from './ProfilePage';

class RouterURL extends Component {
    render() {
        const user = null;
        return (
            user ?
                <ProfilePage />
            :
            <Router>
            <div>
            <Route exact path="/"><Topmenu/><Headermenu/><Categories/><Sale/><New/><Footer/></Route>
            <Route path="/product/:id" ><Topmenu/><Product /><Footer/></Route>
            <Route path="/signIn"><SignIn/></Route>
            <Route path="/signUp"><SignUp/></Route>
            </div>
            </Router>
        );
    }
}

export default RouterURL;