import React, { Component } from 'react';
import * as firebase from "firebase";
import Newproduct from './Newproduct';
import {firebaseData} from "./firebaseConnect.js";
class New extends Component {
  constructor(props) {
    super(props);
    this.state = {
        table:[],
      };    
}
componentWillMount(){
  var dataSanPham = firebase.database().ref("/NewProduct");
  dataSanPham.once("value").then((snapshot)=>{
    this.setState({
      table: snapshot.val()
    });
});
}

    render() {   
        return (
            <div>
  {/*------------new product-------------*/}
  <section className="new-products">
    <div className="container">
      <div className="title-box">
        <h2>New products</h2>
      </div>
      <div className="container">
      <div className="row">

      {(this.state.table).map((value,key)=>{
                return (
                    <Newproduct key={key}
                        tinid={key}
                        anhsp={value.anh.photo1}
                        tensp={value.ten}
                        giasp={value.gia}>
                    </Newproduct>
                    )
            })}
            </div></div>
    </div>
  </section>
  {/*-----------------web features---------------------*/}
  <section className="website-features">
    <div className="container">
      <div className="row">
        <div className="col-md-3 col-6 feature-box text-center">
          <img src="images/feature1.png" alt="anh" />
          <div className="feature-text">
            <p><b> 100% chính hãng</b> có sẵn</p>
          </div>
        </div>
        <div className="col-md-3 col-6 feature-box text-center">
          <img src="images/feature2.jpg" alt="anh" />
          <div className="feature-text">
            <p><b> Hoàn trả trong 7 ngày</b> nếu gặp lỗi</p>
          </div>
        </div>    
        <div className="col-md-3 col-6 feature-box text-center">
          <img src="images/feature3.png" alt="anh"/>
          <div className="feature-text">
            <p><b> Miễn phí vận chuyển</b> cho đơn hàng lớn</p>
          </div>
        </div>
        <div className="col-md-3 col-6 feature-box text-center">
          <img src="images/feature4.png" alt="anh"/>
          <div className="feature-text">
            <p><b> Thanh toán oline </b>nhanh gọn</p>
          </div>
        </div>    
      </div>
    </div>
  </section>
</div>

        );
    }
}

export default New;